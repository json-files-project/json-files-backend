package com.projeto.model;

public class Song {
	
	private String song;
	private String singer;
	public Song() {
		super();
	}
	public Song(String song, String singer) {
		super();
		this.song = song;
		this.singer = singer;
	}
	public String getSong() {
		return song;
	}
	public void setSong(String song) {
		this.song = song;
	}
	public String getSinger() {
		return singer;
	}
	public void setSinger(String singer) {
		this.singer = singer;
	}
	

}
