package com.projeto.controller;

import java.io.File;
import java.io.IOException;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.projeto.model.Song;

@RestController
@RequestMapping("/song")
public class SongController {
	
	static final String PATH = "../json-files-backend/src/main/resources/json/file.json";
	
	@RequestMapping("/")
	public String test(){
		return "Spring is on";
	}
	
	@RequestMapping("/getFile")
	public Song getFile() throws JsonParseException, JsonMappingException, IOException{
		ObjectMapper objectMapper = new ObjectMapper();
		Song song = objectMapper.readValue(new File(PATH), Song.class);
		return song;
	}

}
